# Data_Manipulation_Spark_2


In this py file you can have a look at multiple examples to manipulate data using Spark. You can run the code with google colab (because we use some files from there) or you can run the code on your computer but you'll need to download the cars.csv and README_googlecolab.md files. In the end, we show a few examples of spark dataframes.

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

October 2021

