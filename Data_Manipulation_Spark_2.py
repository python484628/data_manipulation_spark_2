# Author : Marion Estoup 
# E-mail : marion_110@hotmail.fr
# October 2021



# Install pyspark if necessary
!pip install pyspark



# Create a spark context 
from pyspark import SparkContext
sc = SparkContext("local[2]", "First App")

# Stopping Spark context 
sc.stop()

# Configuration 
# * to use all capacity
from pyspark import SparkConf, SparkContext
conf = (SparkConf()
  .setMaster("local[*]") 
  .setAppName("My app") 
  .set("spark.executor.memory", "1g"))
sc = SparkContext(conf = conf)

# Inspect 
sc.pythonVer # retrieve python version

sc.master # master url to connect to

str(sc.sparkHome) # path where psark is installed on worker nodes

str(sc.sparkUser()) # retrieve name of the spark user running sparkcontext

sc.appName # return application name

sc.applicationId # retrieve application ID

sc.defaultParallelism # return defaut level of parallelism

sc.defaultMinPartitions # default min number of partitions for RDDs

"""**Using the shell**

In the pyspark shell, a special interpreter aware sparkcontext is already created in the variable called sc

$./bin/spark-shell--master local[2]

$./bin/pyspark--master local[4] --py-files code.py

Set which master the context connects to with the --master argument, and add python .zip, .egg or .py files to the runtime path by passing comma-separated list to --py-files

**Execution**

$./bin/spark-submit examples/src/main/python/pi.py

**Loading data with RDDs (resilient distributed datasets)**

**External data**

Read either one text file from HDFS, a local file system or any Haddop supported file system URI with textFile(), or read in a directory of text files with whole TextFiles()
"""

# Here we read the file readme from sample_data in google colab
textFile = sc.textFile("sample_data/README.md")

# Or you can download the file and open it from your computer
textFile = sc.textFile("C:/Users/mario/Desktop/README_googlecolab.md")

# Collect the first sentence of the file
textFile.first()

# Collect the whole content of the text file
textFile.collect()


# Here we can get all files that are in sample_data in google colab
textFile2 = sc.wholeTextFiles("sample_data/")

# Content of the first file named anscombe (which is in sample_data from google colab)
textFile2.first() 

"""**Word count with pySpark**

Count the occurences of unique words in a text 
"""

# split each line into words
words = textFile.flatMap(lambda line: line.strip().split(" "))

# Collect each word
words.collect()

# count the number of all words in the document
wordCounts = words.count()
print(wordCounts)

# count the occurrence of each word
wordCounts = words.map(lambda word: (word, 1)).reduceByKey(lambda a,b:a +b)

wordCounts.collect()

# save the result if needed
wordCounts.saveAsTextFile("./wordCounts/")




# Parallelized collections
rdd1 = sc.parallelize([('a',7),('a',2),('b',2)])
rdd2 = sc.parallelize([('a',["x","y","z"]),('b',["p","r"])])
rdd3 = sc.parallelize(range(100))
rdd4 = sc.parallelize([('a',["x","y","z"]),('b',["p","r"])])

"""**Retrieving RDD information**"""

rdd1.getNumPartitions() # List the number of partitions

rdd1.count() # count RDD instances

rdd1.countByKey() # Count RDD instances by key

rdd1.countByValue() # count RDD instance by value

rdd1.collectAsMap() # return (key,value) pairs as a dictionnary

sc.parallelize([]).isEmpty() # Check wether RDD is empty

"""**Summary**"""

print(rdd3.sum()) # Sum of RDD elements
print(rdd3.max()) # Max value of RDD elements
print(rdd3.min()) # Min value of RDD elements
print(rdd3.mean()) # Mean value of RDD elements
print(rdd3.stdev()) # Standard deviation value of RDD elements
print(rdd3.variance()) # Compute variance of RDD elements
print(rdd3.histogram(3)) # Compute histogram by bins
print(rdd3.stats()) # Sumarry statistics count, mean, stdev, max and min

"""**Applying functions**"""

rdd1.map(lambda x: x+(x[1], x[0])).collect() # Apply a function to each RDD element

rdd5 = rdd1.flatMap(lambda x: x+(x[1], x[0])) # apply a flatMap function to each RDD element and flatten the result
rdd5.collect()

rdd4.flatMapValues(lambda x: x).collect() # Apply a flatMap function to each (key,value) pairs

"""**Selecting Data**"""

# Getting
rdd1.collect() # Return a list with all RDD elements

rdd1.first() # Take first RDD element

rdd1.top(2) # Take top 2 RDD elements

rdd1.take(2) # Take 2 first RDD elements

"""**Sampling**"""

rdd3.sample(True, 0.1).collect() # Return sampled subset of rdd3

# Filtering
rdd1.filter(lambda x: "a" in x).collect() # Filter the RDD

rdd5.distinct().collect() # Return distinct RDD values

rdd1.keys().collect() # Return (key,value) RDD's keys

rdd1.values().collect() # RReturn (key,value) RDD's values

# Iterating
def g(x): print(x)
rdd1.foreach(g) # Apply a function to all RDD elements

# Reshaping Data

# Reducing

rdd1.reduceByKey(lambda x,y : x+y).collect() # Merge the RDD values for each key

rdd1.reduce(lambda a,b: a + b) # Merge the RDD values

# Grouping by
rdd3.groupBy(lambda x: x % 2).mapValues(list).collect() # Return RDD of grouped values

rdd1.groupByKey().mapValues(list).collect() # Group RDD by key

# Aggregating
seqOp = (lambda x,y: (x[0]+y, x[1]+1))
combOp = (lambda x,y: (x[0]+y[0], x[1]+y[1]))

rdd1.aggregateByKey((0,0), seqOp,combOp).collect() # Aggregate values of each RDD key

rdd3.aggregate((0,0),seqOp,combOp) # Aggregate RDD elements of each partition and then the results

def add(x,y): return x+y
rdd3.fold(0,add) # Aggregate the elements of each partition, and then the results

rdd1.foldByKey(0,add).collect() # Merge the values for each key

rdd3.keyBy(lambda x: x+x).collect() # Create tuples of RDD elements by applying a function

# Mathematical Operations
rdd1_1 = sc.parallelize([('a',4),('a',2),('b',1)])
print(rdd1.collect())
print(rdd1_1.collect())
print(rdd1.subtract(rdd1_1).collect())

rdd1_1 = sc.parallelize([('a',4)])
rdd1.subtractByKey(rdd1_1).collect() # Return each (key,value) pair of rdd1 with no matching

print(rdd1.collect())
print(rdd2.collect())
rdd1.cartesian(rdd2).collect() # Return the cartesian product of rdd and rdd2

# Sort
rdd2.sortBy(lambda x: x[1]).collect() # Sort RDD by given function

rdd2.sortByKey().collect() # Sort (key,value) RDD by key

# Repartitioning

rdd1.repartition(4) # New RDD with 4 partitions

rdd1.coalesce(1) # Decrease the number of partitions in the RDD to 1

# Save file if needed
rdd1.saveAsTextFile("rdd1.txt")

# Save file as hadoop file
rdd1.saveAsHadoopFile("hdfs://namenodehost/parent/child", 'org.apache.hadoop.mapred.TextOutputFormat')


# stop spark context
sc.stop()




############################################################## Pyspark DataFrames

# Initialyzing pyspark session
from pyspark.sql import SparkSession
spark = SparkSession.builder \
      .master("local[*]") \
      .appName("Cars app") \
      .config("spark.executor.memory", "1g") \
      .getOrCreate()

# Creating a pyspark dataframe
# Example 1
columns = ['ids','dogs','cats']
vals = [(1,2,0), (2,0,1)]
df = spark.createDataFrame(vals, columns)
# Show df
df.show()




# Example 2
from sklearn.datasets import load_breast_cancer
data = load_breast_cancer()
import pandas as pd
pdf = pd.DataFrame(data.data, columns = data.feature_names)

df3 = spark.createDataFrame(pdf)
# Show df
df3.show()



# Example 3
# first download the cars.csv example file (in the repository, on the internet, ...)
#!wget https://perso.telecom-paristech.fr/eagan/class/igr204/data/cars.csv
carslist = spark.read.csv("C:/Users/mario/Desktop/cars.csv", # put here your local path to this file
                           inferSchema = True,
                           header = True,
                           sep=',')

# Show df
carslist.show()


# Stop spark context
spark.stop()
